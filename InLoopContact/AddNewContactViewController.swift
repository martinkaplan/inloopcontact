//
//  AddNewContactViewController.swift
//  InLoopContact
//
//  Created by martin kaplan on 25/08/16.
//  Copyright © 2016 kaplan. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import JGProgressHUD


class AddNewContactViewController: UITableViewController,UITextFieldDelegate,UINavigationControllerDelegate {
    
    
    @IBOutlet weak var nameCell: FormCell!
    @IBOutlet weak var phoneCell: FormCell!
    
    
    @IBOutlet weak var addContactCell: ButtonCell!
    
    var progressHUD:JGProgressHUD! = PPViewUtility.initProgressHud()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44.0
        self.tableView.allowsSelection = false
        
        self.navigationItem.title = "Add New Contact"
        
        self.phoneCell.theTextField.tag = 1
        self.phoneCell.theTextField.delegate = self
        self.nameCell.theTextField.delegate = self
        
        self.addContactCell.theButton.addTarget(self, action: #selector(self.addContactAction(_:)), forControlEvents: .TouchUpInside)
    }
    
    func showLoginAlertWithMessage(title:String, message:String, fn:() -> Void)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .Default, handler: {(alert: UIAlertAction!) in fn()})
        alertController.addAction(OKAction)
        
        self.presentViewController(alertController, animated: true) {}
    }
   
    //MARK: -
    @IBAction func addContactAction(sender: UIButton?)
    {
        guard let fullName:String = self.nameCell.theTextField.text where fullName.characters.count > 4  else {
            self.showLoginAlertWithMessage("Error", message: "Fill user name (min. lenght 5 char)", fn: {})
            return
        }
        guard let phone:String = self.phoneCell.theTextField.text where phone.characters.count > 4 else {
            self.showLoginAlertWithMessage("Error", message: "Fill user phone (min. lenght 5 char)", fn: {})
            return
        }
        
        self.progressHUD.showInView(self.view, animated: true)
        
        InLoopApiClient.sharedInstance.addNewContact(withName: fullName, phone: phone)
        { (result) in
                self.progressHUD.dismissAnimated(true)
                
                switch result{
                case .Failure(let error):
                    print(error.localizedDescription)
                    self.showLoginAlertWithMessage("Error", message: error.localizedDescription, fn: {})
                    
                case .Success(let newContact):
                    
                    print("new contact created \(newContact.name)")
                    self.showLoginAlertWithMessage("New contact added", message: "", fn: {
                        NSNotificationCenter.defaultCenter().postNotificationName(ILConstants.newContactNotifi, object: nil)

                        self.navigationController?.popViewControllerAnimated(true)
                    })
                }
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.row{
        case 1:
            self.nameCell.theTextField.becomeFirstResponder()
        case 2:
            self.phoneCell.theTextField.becomeFirstResponder()
        default:
            break
        }
    }
    
    //MARK: - textfield
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        switch textField.tag{
        case 0:
            self.phoneCell.theTextField.becomeFirstResponder()
        case 1:
            self.phoneCell.theTextField.endEditing(true)
            self.addContactAction(nil)
        default:
            break
        }
        return true
    }
    
}
