//
//  ContactsViewController.swift
//  InLoopContact
//
//  Created by martin kaplan on 24/08/16.
//  Copyright © 2016 kaplan. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage
import JGProgressHUD


class ContactsViewController: UITableViewController
{
    internal struct ClassConstants {
        static let segueToAddContact = "segueToAddContact"
        static let segueToContactDetail = "segueToContactDetail"
        
        static let keyContacts = "keyContacts"
    }
    var progressHUD:JGProgressHUD! = PPViewUtility.initProgressHud()
    
    var contacts:[Contact] = []{
        didSet{
            self.tableView.reloadData()
        }
    }
    var selectedContactIndex:Int = 0
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView()
        self.tableView.backgroundColor = ILConstants.colorWhite
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 95.0
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.loadContacts), forControlEvents: UIControlEvents.ValueChanged)
        refreshControl.tintColor = ILConstants.colorBlack
        self.refreshControl = refreshControl

        self.attemptToLoadContactsFromDefaults()
        
        self.loadContacts()
        
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: #selector(newContactAdded),
            name: ILConstants.newContactNotifi,
            object: nil)
    }
    
    private func attemptToLoadContactsFromDefaults()
    {
        if UIApplication.sharedApplication().protectedDataAvailable{
            if let cachedAppInfoData:NSData = NSUserDefaults.standardUserDefaults().objectForKey(ClassConstants.keyContacts) as? NSData{
                if let contacts:[Contact] =  NSKeyedUnarchiver.unarchiveObjectWithData(cachedAppInfoData) as? [Contact]{
                    self.contacts = contacts
                }
            }
        }
    }
    
    func newContactAdded(notification: NSNotification){
        self.loadContacts()
    }
    
    func loadContacts()
    {
        self.refreshControl?.endRefreshing()
        if !self.progressHUD.visible{
            self.progressHUD.showInView(self.view, animated: true)
        }
        InLoopApiClient.sharedInstance.loadContacts { (result) in
            
            self.progressHUD.dismissAnimated(true)
            
            switch result{
            case .Success(let newContacts):
                self.contacts = newContacts
                
                let data = NSKeyedArchiver.archivedDataWithRootObject(newContacts)
                NSUserDefaults.standardUserDefaults().setObject(data, forKey: ClassConstants.keyContacts)
                
            case .Failure(let error):
                let alertController = UIAlertController(title: "Error", message:error.localizedDescription, preferredStyle: .Alert)
                
                alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (UIAlertAction) in
                    self.loadContacts()
                }))
                self.presentViewController(alertController, animated: true) {}
            }
        }
    }
    
    //MARK: - tableview
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return contacts.count
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        self.selectedContactIndex = indexPath.row
        self.performSegueWithIdentifier(ClassConstants.segueToContactDetail, sender: self)
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("ContactCell", forIndexPath: indexPath) as! ContactCell
        
        let contact = self.contacts[indexPath.row]
        cell.nameLabel.text = contact.name
        cell.phoneLabel.text = contact.phone
        
        
        cell.theImageView.image = UIImage(named: "placeholder")
        
        if let pictUrl = contact.pictureUrl{
            
            if validateUrl(pictUrl){
                self.loadImageToImageView(pictUrl, imageView: cell.theImageView)
            }else{
                self.loadImageToImageView( UrlManager.imgageUrl + pictUrl, imageView: cell.theImageView)
            }
        }
        return cell
    }
    
    func validateUrl (urlString: String?) -> Bool {
        let urlRegEx = "(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
        return NSPredicate(format: "SELF MATCHES %@", urlRegEx).evaluateWithObject(urlString)
    }
    
    func loadImageToImageView(imgUrl:String, imageView:UIImageView)
    {
        if let url = NSURL(string:imgUrl){
            imageView.af_setImageWithURL(
                url,
                placeholderImage: UIImage(named: "placeholder"),
                filter: nil,
                imageTransition: .FlipFromBottom(0.333))
        }
    }
    
    //MARK: -
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        super.prepareForSegue(segue, sender: sender)
        switch segue.identifier!{
        case ClassConstants.segueToAddContact:
            break
        case ClassConstants.segueToContactDetail:
            if let contactDetailVC:ContactDetailViewController = segue.destinationViewController as? ContactDetailViewController{
                contactDetailVC.contact = self.contacts[self.selectedContactIndex]
            }
        default:
            print("OrdersVC unknown segue \(segue.identifier)")
            break
        }
    }
    
    @IBAction func addContactAction(sender: AnyObject)
    {
        self.performSegueWithIdentifier(ClassConstants.segueToAddContact, sender: self)
    }
    
}
