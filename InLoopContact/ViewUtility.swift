//
//  ViewUtility.swift
//  InLoopContact
//
//  Created by martin kaplan on 25/08/16.
//  Copyright © 2016 kaplan. All rights reserved.
//

import Foundation
import JGProgressHUD


class PPViewUtility
{
    
    static func setupButton(button:UIButton)
    {
        self.roundCornersAt(button, radiuds: ILConstants.cornerRadius)
        button.titleLabel?.font = ILConstants.fontSemiBold(ILConstants.sizeLarge)
        
        button.setTitleColor(ILConstants.colorBlack, forState: UIControlState.Normal)
        button.backgroundColor = ILConstants.colorWhite
        button.layer.borderColor = ILConstants.colorBlack.CGColor
        button.layer.borderWidth = 1
    }

    
    //MARK: - HUD
    static func initProgressHud() -> JGProgressHUD
    {
        let progressHUD = JGProgressHUD(style: JGProgressHUDStyle.Dark)
        progressHUD.HUDView.layer.shadowOffset = CGSizeZero
        progressHUD.HUDView.layer.shadowOpacity = 0.4
        progressHUD.HUDView.layer.shadowRadius = 8.0
        progressHUD.animation = JGProgressHUDFadeZoomAnimation()
        
        return progressHUD
    }
    
    
    //MARK: - alert
    static func showAlertWithMessage(title:String, message:String, fn:() -> Void, viewControler:UIViewController)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        
        let OKAction = UIAlertAction(title: "OK", style: .Default, handler: {(alert: UIAlertAction!) in fn()})
        
        alertController.addAction(OKAction)
        
        viewControler.presentViewController(alertController, animated: true) {}
    }
    
    //MARK: -
    static func makeViewRound(view:UIView)
    {
        self.roundCornersAt(view, radiuds: view.frame.width/2)
    }
    
    static func unRoundView(view:UIView)
    {
        view.layer.cornerRadius = 0
        view.layer.masksToBounds = false
    }
    
    static func roundCornersAt(view:UIView, radiuds:CGFloat)
    {
        view.layer.cornerRadius = radiuds
        view.layer.masksToBounds = true
    }
    
}

