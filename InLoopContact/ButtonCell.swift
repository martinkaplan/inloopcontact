//
//  ButtonCell.swift
//  InLoopContact
//
//  Created by martin kaplan on 25/08/16.
//  Copyright © 2016 kaplan. All rights reserved.
//

import UIKit

class ButtonCell: UITableViewCell {
    
    @IBOutlet weak var theButton: UIButton!
    
    
    override func awakeFromNib() {
        self.selectedBackgroundView?.backgroundColor = ILConstants.colorWhite
        PPViewUtility.setupButton(self.theButton)
    }
    
}