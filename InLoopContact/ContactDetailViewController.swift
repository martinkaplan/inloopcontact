//
//  ContactDetailViewController.swift
//  InLoopContact
//
//  Created by martin kaplan on 24/08/16.
//  Copyright © 2016 kaplan. All rights reserved.
//

import Foundation
import UIKit
import MessageUI


class ContactDetailViewController: UITableViewController, MFMessageComposeViewControllerDelegate
{
    var contact:Contact!
    var orders:[Order]?{
        didSet{
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 45.0
        self.tableView.backgroundColor = ILConstants.colorWhite
        
        self.loadOrders()
        
        self.navigationItem.title = self.contact.name
        self.tableView.allowsSelection = false
    }
    
    
    func loadOrders()
    {
        InLoopApiClient.sharedInstance.loadOrders(withIdentifier: self.contact.identifier, callback:
            { (result) in
                switch result{
                case .Success(let newOrders):
                    self.orders = newOrders
                    
                case .Failure(let error):
                    let alertController = UIAlertController(title: "Error", message:error.localizedDescription, preferredStyle: .Alert)
                    
                    alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (UIAlertAction) in
                        self.loadOrders()
                    }))
                    self.presentViewController(alertController, animated: true) {}
                }
        })
    }
    
    
    //MARK: - tableview
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if let orders = self.orders{
            return orders.count
        }
        return 0
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("RightDetailCell", forIndexPath: indexPath)
        
        if let order = self.orders?[indexPath.row]{
            cell.textLabel?.text = order.name
            cell.detailTextLabel?.text = "\(order.count)x"
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("SubtitleCell")
        cell?.contentView.backgroundColor = ILConstants.colorLightGray
        
        cell?.textLabel?.text = "Phone"
        cell?.textLabel?.backgroundColor = ILConstants.colorClear
        cell?.textLabel?.font = ILConstants.fontSemiBold(ILConstants.sizeNormal)
        
        cell?.detailTextLabel?.text = self.contact.phone
        cell?.detailTextLabel?.backgroundColor = ILConstants.colorClear
        cell?.textLabel?.font = ILConstants.fontRegular(ILConstants.sizeNormal)
        
        
        let actionButton = UIButton()
        if let frame = cell?.frame{
            actionButton.frame = frame
        }
        actionButton.addTarget(self, action: #selector(self.handlePhoneClick), forControlEvents: .TouchUpInside)
        
        cell?.addSubview(actionButton)
        
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if self.contact.phone != nil{
            return 55
        }
        return 0
    }
    
    func handlePhoneClick(){
        if MFMessageComposeViewController.canSendText(){
            let messageComposeVC = MFMessageComposeViewController()
            messageComposeVC.messageComposeDelegate = self  //  Make sure to set this property to self, so that the controller can be dismissed!
            if let phone = self.contact.phone{
                messageComposeVC.recipients = [phone]
            }
            messageComposeVC.body = "Hey friend - Just sending a text message in-app using Swift!"
            presentViewController(messageComposeVC, animated: true, completion: nil)

        }
    }
    
    // MFMessageComposeViewControllerDelegate
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}
