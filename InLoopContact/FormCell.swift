//
//  FormCell.swift
//  InLoopContact
//
//  Created by martin kaplan on 25/08/16.
//  Copyright © 2016 kaplan. All rights reserved.
//

import UIKit


class FormCell: UITableViewCell {
    
    @IBOutlet weak var theTextField: UITextField!
    @IBOutlet weak var theLabel: UILabel!
    @IBOutlet weak var theView: UIView!
    
    
    override func awakeFromNib() {
        
        theLabel.textColor = ILConstants.colorDarkGray
        theLabel.font = ILConstants.fontRegular(ILConstants.sizeNormal)

        
    }
    
}