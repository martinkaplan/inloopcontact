//
//  UrlManager.swift
//  InLoopContact
//
//  Created by martin kaplan on 24/08/16.
//  Copyright © 2016 kaplan. All rights reserved.
//

import Foundation


class UrlManager{
    
    private static var serverUrl = "https://inloop-contacts.appspot.com/_ah/api/"
    
    static var contactsUrl:String{
        return serverUrl + "contactendpoint/v1/contact"
    }
    
    static func ordersUrl(contactId:String)->String{
        return serverUrl + "orderendpoint/v1/order/" + contactId
    }
    
    
    static var imgageUrl:String{
        return "http://inloop-contacts.appspot.com/"
    }
    
}