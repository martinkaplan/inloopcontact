//
//  Contact.swift
//  InLoopContact
//
//  Created by martin kaplan on 24/08/16.
//  Copyright © 2016 kaplan. All rights reserved.
//

import Foundation
import SwiftyJSON


@objc class Contact: NSObject, NSCoding {
    
    let identifier:String
    let name:String
    let kind:String
    
    let phone:String?
    let pictureUrl:String?
    
    
    init?(json: SwiftyJSON.JSON)
    {
        if let idStr = json["id"].string {
            self.identifier = idStr
        }else{
            return nil
        }
        
        if let nameStr = json["name"].string {
            self.name = nameStr
        }else{
            return nil
        }
        
        if let kindStr = json["kind"].string {
            self.kind = kindStr
        }else{
            return nil
        }
        
        if let phoneStr = json["phone"].string {
            self.phone = phoneStr
        }else{
            self.phone = nil
        }
        
        if let pictureUrlStr = json["pictureUrl"].string {
            self.pictureUrl = pictureUrlStr
        }else{
            self.pictureUrl = nil
        }
    }
    
    //MARK: - NSCoding
   private struct ContactKeys{
        static let identifierKey = "identifier"
        static let nameKey = "nameKey"
        static let kindKey = "kindKey"
        static let phoneKey = "phoneKey"
        static let pictureUrlKey = "pictureUrlKey"
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        guard let _name = aDecoder.decodeObjectForKey(ContactKeys.nameKey) as? String else { return nil }
        guard let _identifier = aDecoder.decodeObjectForKey(ContactKeys.identifierKey) as? String else { return nil }
        guard let _kind = aDecoder.decodeObjectForKey(ContactKeys.kindKey) as? String else { return nil }
        
        self.identifier = _identifier
        self.name = _name
        self.kind = _kind
        
        self.pictureUrl = aDecoder.decodeObjectForKey(ContactKeys.pictureUrlKey) as? String
        self.phone = aDecoder.decodeObjectForKey(ContactKeys.phoneKey) as? String
        
        super.init()
    }
    
    func encodeWithCoder(aCoder: NSCoder)
    {
        aCoder.encodeObject(self.name, forKey: ContactKeys.nameKey)
        aCoder.encodeObject(self.identifier, forKey: ContactKeys.identifierKey)
        aCoder.encodeObject(self.kind, forKey: ContactKeys.kindKey)
        
        if let pictUrl = self.pictureUrl {
            aCoder.encodeObject(pictUrl, forKey: ContactKeys.pictureUrlKey)
        }
        if let tell = self.phone {
            aCoder.encodeObject(tell, forKey: ContactKeys.phoneKey)
        }
    }
    
}
