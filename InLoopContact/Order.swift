//
//  Order.swift
//  InLoopContact
//
//  Created by martin kaplan on 24/08/16.
//  Copyright © 2016 kaplan. All rights reserved.
//

import Foundation
import SwiftyJSON


struct Order{
    
    let count:Int
    let name:String
    
    
    init?(json: SwiftyJSON.JSON)
    {
        if let idStr = json["count"].int {
            self.count = idStr
        }else{
            return nil
        }
        
        if let nameStr = json["name"].string {
            self.name = nameStr
        }else{
            return nil
        }
      
    }
    
}
