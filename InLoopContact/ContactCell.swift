//
//  ContactCell.swift
//  InLoopContact
//
//  Created by martin kaplan on 24/08/16.
//  Copyright © 2016 kaplan. All rights reserved.
//

import UIKit

class ContactCell: UITableViewCell {
    
    @IBOutlet weak var theImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    override func awakeFromNib() {
        
        nameLabel.font = ILConstants.fontSemiBold(ILConstants.sizeNormal)
        phoneLabel.font = ILConstants.fontRegular(ILConstants.sizeSmall)
        
    }
    
}