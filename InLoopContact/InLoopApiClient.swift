//
//  InLoopApiClient.swift
//  InLoopContact
//
//  Created by martin kaplan on 24/08/16.
//  Copyright © 2016 kaplan. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


public enum ApiResult<T>
{
    case Success(T)
    case Failure(NSError)
}


///////////////////////////////////////////////////
class InLoopApiClient : NSObject {
    
    
    class var sharedInstance: InLoopApiClient {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: InLoopApiClient? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = InLoopApiClient()
        }
        return Static.instance!
    }
    
    
    private override init() {
        super.init()
    }
    
    
    func loadContacts(callback callback: (result: ApiResult<[Contact]>)->Void)
    {
        Alamofire.request(.GET, UrlManager.contactsUrl).responseJSON
            { response in
                if response.result.isSuccess{
                    
                    let json = JSON(response.result.value as! NSDictionary)
                    //  print(json)
                    if let contactsJSON = json["items"].array{
                        var contacts:[Contact] = []
                        for contactJson in contactsJSON{
                            if let contact = Contact(json: contactJson){
                                contacts.append(contact)
                            }
                        }
                        callback(result: .Success(contacts))
                    }else{
                        callback(result: .Failure(NSError(domain: "No contacts", code: 1, userInfo: nil)))
                    }
                    
                }else{
                    callback(result: .Failure(response.result.error!))
                }
        }
    }
    
    //MARK: -
    func loadOrders(withIdentifier identifier:String, callback: (result: ApiResult<[Order]>)->Void)
    {
        Alamofire.request(.GET, UrlManager.ordersUrl(identifier)).responseJSON
            { response in
                if response.result.isSuccess{
                    
                    let json = JSON(response.result.value as! NSDictionary)
              //      print(json)
                    if let ordersJson = json["items"].array{
                        var orders:[Order] = []
                        for ordersJson in ordersJson{
                            if let order = Order(json: ordersJson){
                                orders.append(order)
                            }
                        }
                        callback(result: .Success(orders))
                    }else{
                        callback(result: .Failure(NSError(domain: "No orders", code: 1, userInfo: nil)))
                    }
                    
                }else{
                    print(response.result.error)
                    callback(result: .Failure(response.result.error!))
                }
        }
    }
    
    //MARK: -
    func addNewContact(withName name:String, phone:String, callback: (result: ApiResult<Contact>)->Void)
    {
        let parameters:[String:AnyObject] = [
            "name":name,
            "phone":phone]
        
        Alamofire.request(.POST, UrlManager.contactsUrl, parameters: parameters, encoding: .JSON).responseJSON
            { response in
                if response.result.isSuccess{
                    let json = JSON(response.result.value as! NSDictionary)
                  //  print(json)
                    if let contact = Contact(json: json){
                        callback(result: .Success(contact))
                    }else{
                        callback(result: .Failure(NSError(domain: "Something went wrong", code: 1, userInfo: nil)))
                    }
                }else{
                    callback(result: .Failure(response.result.error!))
                }
        }
    }


}