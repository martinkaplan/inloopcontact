//
//  ILConstants.swift
//  InLoopContact
//
//  Created by martin kaplan on 24/08/16.
//  Copyright © 2016 kaplan. All rights reserved.
//

import Foundation
import UIKit

struct ILConstants
{
    static var cornerRadius:CGFloat = 5

    static var newContactNotifi = "newContactNotifi"

    private static var iphone = "iPhone"
    static var sizeSmall:CGFloat = UIDevice.currentDevice().modelName.containsString(iphone) ? 13 : 15
    static var sizeNormal:CGFloat = UIDevice.currentDevice().modelName.containsString(iphone) ? 15 : 17
    static var sizeLarge:CGFloat = UIDevice.currentDevice().modelName.containsString(iphone) ? 17 : 19
    
    
    //MARK: - colors
    static var colorWhite:UIColor{
        return UIColor.whiteColor()
    }
    
    static var colorBlack:UIColor{
        return UIColor.blackColor()
    }
    
    static var colorLightGray:UIColor{
        return UIColor.groupTableViewBackgroundColor()
    }
    
    static var colorDarkGray:UIColor{
        return UIColor.darkGrayColor()
    }
    
    static var colorBlue:UIColor{
        return UIColor(red: 55/255.0, green: 98/255.0, blue: 182/255.0, alpha: 1.0)
    }
    
    static var colorClear:UIColor{
        return UIColor.clearColor()
    }
    
    
    //MARK: - fonts
    static func fontSemiBold(size:CGFloat)->UIFont{
        if #available(iOS 8.2, *) {
            return UIFont.systemFontOfSize(size, weight: UIFontWeightSemibold)
        } else {
           return UIFont(name: "HelveticaNeue-SemiBold", size: size)!
        }
    }
    
    static func fontMedium(size:CGFloat)->UIFont{
        if #available(iOS 8.2, *) {
            return UIFont.systemFontOfSize(size, weight: UIFontWeightMedium)
        } else {
            return UIFont(name: "HelveticaNeue-Medium", size: size)!
        }
    }
    
    static func fontRegular(size:CGFloat)->UIFont{
        if #available(iOS 8.2, *) {
            return UIFont.systemFontOfSize(size, weight: UIFontWeightRegular)
        } else {
            return UIFont(name: "HelveticaNeue", size: size)!
        }
    }
    
}


